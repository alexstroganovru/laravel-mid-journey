<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Mid Journey API Key and chat id
    |--------------------------------------------------------------------------
    |
    | Here you may specify your Mid-Journey API Key and organization.
    */

    'api_key' => env('MID_JOURNEY_API_KEY'),

];
