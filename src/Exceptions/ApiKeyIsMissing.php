<?php

declare(strict_types=1);

namespace AlexStroganovRu\LaravelMidJourney\Exceptions;

use InvalidArgumentException;

final class ApiKeyIsMissing extends InvalidArgumentException
{
    public static function create(): self
    {
        return new self(
            'The Mid Journey API Key is missing. Please publish the [midjourneyai.php] configuration file and set the [api_key].'
        );
    }
}
