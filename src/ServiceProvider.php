<?php

declare(strict_types=1);

namespace AlexStroganovRu\LaravelMidJourney;

use AlexStroganovRu\LaravelMidJourney\Exceptions\ApiKeyIsMissing;
use AlexStroganovRu\MidJourneyAI\Client;
use AlexStroganovRu\MidJourneyAI\Contracts\ClientContract;
use AlexStroganovRu\MidJourneyAI\MidJourneyAI;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;

final class ServiceProvider extends BaseServiceProvider implements DeferrableProvider
{
    public function register(): void
    {
        $this->app->singleton(ClientContract::class, static function (): Client {
            $apiKey = config('midjourneyai.api_key');

            if (! is_string($apiKey)) {
                throw ApiKeyIsMissing::create();
            }

            return MidJourneyAI::client($apiKey);
        });

        $this->app->alias(ClientContract::class, 'midjourneyai');
        $this->app->alias(ClientContract::class, Client::class);
    }

    public function boot(): void
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__.'/../config/midjourneyai.php' => config_path('midjourneyai.php'),
            ]);
        }
    }

    public function provides(): array
    {
        return [
            Client::class,
            ClientContract::class,
            'midjourneyai',
        ];
    }
}
