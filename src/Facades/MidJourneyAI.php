<?php

declare(strict_types=1);

namespace AlexStroganovRu\LaravelMidJourney\Facades;

use Illuminate\Support\Facades\Facade;

class MidJourneyAI extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return 'midjourneyai';
    }
}