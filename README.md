## Mid Journey API

**MidJourney PHP** for Laravel is a API client that allows you to interact with the [MidJourney AI API]. If you or your
business relies on this package, it's important to support the developers who have contributed their time and effort to
create and maintain this valuable tool:

- Alex Stroganov: **[gitlab.com/alexstroganovru](https://gitlab.com/alexstroganovru)**

> **Note:** This repository contains the integration code of the **MidJourneyAI PHP** for Laravel. If you want to use the  **MidJourneyAI PHP** client in a framework-agnostic way, take a look at the [alexstroganovru/client-mid-journey](https://gitlab.com/alexstroganovru/client-mid-journey) repository.

## Table of Contents

- [Get Started](#get-started)
- [Usage](#usage)
- [Testing](#testing)

## Get Started

> **Requires [PHP 8.2+](https://php.net/releases/)**

First, install MidJourneyAI via the [Composer](https://getcomposer.org/) package manager:

```bash
composer require alexstroganovru/laravel-mid-journey
```

Next, publish the configuration file:

```bash
php artisan vendor:publish --provider="AlexStroganovRu\LaravelMidJourney\ServiceProvider"
```

This will create a `config/midjourneyai.php` configuration file in your project, which you can modify to your needs
using environment variables:

```env
MID_JOURNEY_API_KEY=...
```

Finally, you may use the `MidJourneyAI` facade to access the MidJourneyAI API:

```php
use AlexStroganovRu\LaravelMidJourney\Facades\MidJourneyAI;

$result = MidJourneyAI::messages()->imagine()->generate(CreateRequest::from([
    'channel_id' => 'YOUR_CHANNEL_ID',
    'prompt' => 'YOUR_PROMPT',
]));

echo $result;
```

## Usage

For usage examples, take a look at
the [alexstroganovru/client-mid-journey](https://gitlab.com/alexstroganovru/client-mid-journey) repository.

## Testing

For more testing examples, take a look at
the [alexstroganovru/client-mid-journey](https://gitlab.com/alexstroganovru/client-mid-journey) repository.

---

MidJourneyAI PHP for Laravel is an open-sourced software licensed under the **[MIT license](https://opensource.org/licenses/MIT)**.